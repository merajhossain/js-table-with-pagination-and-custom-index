import logo from './logo.svg';
import './App.css';
import data from "./data/data";
import Table from "./components/Table";
function App() {

  const tableHead = ["Class Name","Shift Name","Section Name", "Section Name"];
  const tableColumns = ["className","shiftName", "sectionName", "sectionName"];
  return (
    <div className="App">
      <h1>Js table with pagination and custom header</h1>
      <Table data={data} columns={tableColumns} rowsPerPage={4} tableHead={tableHead}/>
    </div>
  );
}

export default App;
