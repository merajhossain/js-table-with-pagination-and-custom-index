import React, { useEffect, useState } from "react";

import useTable from "../../hooks/useTable";
import styles from "./Table.module.css";
import TableFooter from "./TableFooter";

const Table = ({ data, rowsPerPage, tableHead, columns }) => {
  const [page, setPage] = useState(1);
  const [datas,setdatas] = useState([]);
   
    useEffect(()=>{
      const datasx = data.filter(item => {
        Object.keys(item).filter(key => 
          !columns.includes(key))
          .forEach(key => delete item[key]);
          return item
        });
      setdatas(datasx)
    },[data])

  const { slice, range } = useTable(datas, page, rowsPerPage);

  return (
    <>
      <table className={styles.table}>
        <thead className={styles.tableRowHeader}>
          <tr>
            {tableHead.map((item) => {
              return(
                <th className={styles.tableHeader}>{item}</th>
              )
            })}
          </tr>
        </thead>
        <tbody>
          {slice.map((el, index) => (
            <tr className={styles.tableRowItems} key={el.id}>
              {columns.map((item, i) => {
                  return <td className={styles.tableCell}>{el[item]} </td>
              })}
            </tr>
          ))}
        </tbody>
      </table>
      <TableFooter range={range} slice={slice} setPage={setPage} page={page} />
    </>
  );
};

export default Table;
